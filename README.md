# Squid Proxy

Alpine based squid proxy. It only accepts basic authentication that defined in /etc/squid/htpwds
file.

- Run proxy container:
```
$ docker run --name mysquid -p 3128:3128 -i -t -d secopstech/squid-proxy:latest
```

- Create a proxy account:

```
$ docker exec -it squid htpasswd -c /etc/squid/htpwds myuser
$ docker exec -it squid squid -k reconfigure
```